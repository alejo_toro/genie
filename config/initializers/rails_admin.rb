RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.excluded_models = ["BlogitComments", "BlogitPosts" ,"BlogoPosts" ,"BlogoTaggings" ,"BlogoTags" ,"BlogoUsers"]

  config.model 'Section' do

    list do
      field :title
      field :header_title
      field :position
      field :section_type
      field :background_color
      field :title_color
      field :text_color
      field :content
      field :bg_separator
      field :color_separator
    end

    edit do
      field :title
      field :header_title
      field :image, :carrierwave
      field :image_class
      field :section_type, :enum
      field :background_color
      field :title_color
      field :text_color
      field :position
      field :section_blank, :boolean
      field :content, :ck_editor
      field :bg_separator, :carrierwave
      field :color_separator
      field :genies_title
      field :allies_title
      field :clients_title
    end
  end

  config.model 'User' do

    group :change_password do
      label "Change password"
      field :password
      field :password_confirmation
    end

    list do
      field :email
      field :role
      exclude_fields :password, :password_confirmation
    end

    edit do
      field :email
      field :role
      field :password
      field :password_confirmation
    end
  end

  config.model 'Carrousel' do

    list do
      field :title
      field :position
      field :carousel_content
    end

    edit do
      field :title
      field :carousel_content, :ck_editor
      field :position
    end
  end

  config.model 'Factory' do

    list do
      field :name
      field :factory_image
      field :bg_color
      field :position
      field :title
      field :description
      field :factory_bg
    end

    edit do
      field :name
      field :factory_image, :carrierwave
      field :bg_color
      field :position
      field :title
      field :description, :ck_editor
      field :factory_bg, :carrierwave
    end
  end

  config.model 'Client' do

    list do
      field :name
      field :client_logo
      field :link
    end

    edit do
      field :name
      field :client_logo, :carrierwave
      field :link
    end
  end

  config.model 'Ally' do

    list do
      field :name
      field :ally_logo
      field :link
    end

    edit do
      field :name
      field :ally_logo, :carrierwave
      field :link
    end
  end

  config.model 'People' do

    list do
      field :name
      field :picture
      field :role
      field :passion
      field :linkedin_url
      field :position
    end

    edit do
      field :name
      field :picture, :carrierwave
      field :role
      field :passion, :ck_editor
      field :linkedin_url
      field :position
    end
  end

  config.model 'Header' do

    list do
      field :bg_color
      field :bg_image
      field :link_color
      field :link_hover_color
      field :links_width
      field :normal_image
      field :thin_image
      field :border_color
    end

    edit do
      field :bg_color
      field :bg_image, :carrierwave
      field :link_color
      field :link_hover_color
      field :links_width
      field :normal_image, :carrierwave
      field :thin_image, :carrierwave
      field :border_color
    end
  end

  config.model 'Note' do

    list do
      field :bg_color
      field :text_color
    end

    edit do
      field :bg_color
      field :text_color
    end
  end

  config.model 'Address' do

    list do
      field :main_address
      field :address_line_1
      field :address_line_2
      field :lat
      field :lng
      field :city
      field :phone
      field :mobile
      field :contact_email
    end

    edit do
      field :main_address
      field :address_line_1
      field :address_line_2
      field :lat
      field :lng
      field :city
      field :phone
      field :mobile
      field :contact_email
    end
  end

  config.model 'TablePayLink' do
    list do
      field :link
    end

    edit do
      field :link
    end
  end

end
