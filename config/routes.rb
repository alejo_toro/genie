Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  Blogo::Routes.mount_to(self, at: '/blog')
  resources :sections
  root to: 'sections#index'
  match 'contact' => 'sections#contact', :as => :contact, :via =>  :post
  devise_for :users
end
