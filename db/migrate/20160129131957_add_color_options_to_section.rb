class AddColorOptionsToSection < ActiveRecord::Migration
  def change
  	add_column :sections, :background_color, :string
  	add_column :sections, :title_color, :string
  	add_column :sections, :text_color, :string
  end
end
