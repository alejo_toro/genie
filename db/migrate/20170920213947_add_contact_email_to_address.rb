class AddContactEmailToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :contact_email, :string
  end
end
