class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.string :picture
      t.string :rol
      t.text :passion
      t.string :linkedin_url

      t.timestamps null: false
    end
  end
end
