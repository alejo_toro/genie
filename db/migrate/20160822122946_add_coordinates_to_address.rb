class AddCoordinatesToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :lat, :string
    add_column :addresses, :lng, :string
  end
end
