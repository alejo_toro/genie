class AddBgToImageAndTextInCarousel < ActiveRecord::Migration
  def change
  	add_column :carrousels, :bg_image, :string
  	add_column :carrousels, :bg_text, :string
  end
end
