class AddBorderColorToHeaders < ActiveRecord::Migration
  def change
    add_column :headers, :border_color, :string
  end
end
