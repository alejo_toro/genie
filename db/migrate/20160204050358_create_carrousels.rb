class CreateCarrousels < ActiveRecord::Migration
  def change
    create_table :carrousels do |t|
      t.string :carrousel_image
      t.integer :position
      t.string :title
      t.text :description
      t.integer :text_alignment
      t.string :title_color
      t.string :text_color

      t.timestamps null: false
    end
  end
end
