class CreateHeaders < ActiveRecord::Migration
  def change
    create_table :headers do |t|
      t.string :bg_color
      t.string :bg_image
      t.string :link_color
      t.string :link_hover_color
      t.string :links_width
      t.string :normal_image
      t.string :thin_image

      t.timestamps null: false
    end
  end
end
