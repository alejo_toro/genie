class AddBgSeparatorAndColorSeparatorToSection < ActiveRecord::Migration
  def change
    add_column :sections, :bg_separator, :string
    add_column :sections, :color_separator, :string
  end
end
