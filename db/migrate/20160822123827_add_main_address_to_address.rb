class AddMainAddressToAddress < ActiveRecord::Migration
  def change
  	add_column :addresses, :main_address, :boolean, :default => false
  end
end
