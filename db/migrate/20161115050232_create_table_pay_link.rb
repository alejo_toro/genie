class CreateTablePayLink < ActiveRecord::Migration
  def change
    create_table :table_pay_links do |t|
      t.string :link
    end
  end
end
