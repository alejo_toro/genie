class CreateFactories < ActiveRecord::Migration
  def change
    create_table :factories do |t|
      t.string :name
      t.string :factory_image
      t.integer :position
      t.string :title
      t.text :description
      t.string :factory_bg

      t.timestamps null: false
    end
  end
end
