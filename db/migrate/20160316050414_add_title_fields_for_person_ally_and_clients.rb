class AddTitleFieldsForPersonAllyAndClients < ActiveRecord::Migration
  def change
    add_column :sections, :genies_title, :string
    add_column :sections, :allies_title, :string
    add_column :sections, :clients_title, :string
  end
end
