class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.string :bg_color
      t.string :text_color

      t.timestamps null: false
    end
  end
end
