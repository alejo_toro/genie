# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170920213947) do

  create_table "addresses", force: :cascade do |t|
    t.string   "address_line_1", limit: 255
    t.string   "address_line_2", limit: 255
    t.string   "city",           limit: 255
    t.string   "phone",          limit: 255
    t.string   "mobile",         limit: 255
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "lat",            limit: 255
    t.string   "lng",            limit: 255
    t.boolean  "main_address",               default: false
    t.string   "contact_email",  limit: 255
  end

  create_table "allies", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "ally_logo",  limit: 255
    t.string   "link",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "blogit_comments", force: :cascade do |t|
    t.string   "name",       limit: 255,   null: false
    t.string   "email",      limit: 255,   null: false
    t.string   "website",    limit: 255
    t.text     "body",       limit: 65535, null: false
    t.integer  "post_id",    limit: 4,     null: false
    t.string   "state",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blogit_comments", ["post_id"], name: "index_blogit_comments_on_post_id", using: :btree

  create_table "blogit_posts", force: :cascade do |t|
    t.string   "title",          limit: 255,                     null: false
    t.text     "body",           limit: 65535,                   null: false
    t.string   "state",          limit: 255,   default: "draft", null: false
    t.integer  "comments_count", limit: 4,     default: 0,       null: false
    t.integer  "blogger_id",     limit: 4
    t.string   "blogger_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "description",    limit: 65535
  end

  add_index "blogit_posts", ["blogger_type", "blogger_id"], name: "index_blogit_posts_on_blogger_type_and_blogger_id", using: :btree

  create_table "blogo_posts", force: :cascade do |t|
    t.integer  "user_id",          limit: 4,     null: false
    t.string   "permalink",        limit: 255,   null: false
    t.string   "title",            limit: 255,   null: false
    t.boolean  "published",                      null: false
    t.datetime "published_at",                   null: false
    t.string   "markup_lang",      limit: 255,   null: false
    t.text     "raw_content",      limit: 65535, null: false
    t.text     "html_content",     limit: 65535, null: false
    t.text     "html_overview",    limit: 65535
    t.string   "tags_string",      limit: 255
    t.string   "meta_description", limit: 255,   null: false
    t.string   "meta_image",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blogo_posts", ["permalink"], name: "index_blogo_posts_on_permalink", unique: true, using: :btree
  add_index "blogo_posts", ["published_at"], name: "index_blogo_posts_on_published_at", using: :btree
  add_index "blogo_posts", ["user_id"], name: "index_blogo_posts_on_user_id", using: :btree

  create_table "blogo_taggings", force: :cascade do |t|
    t.integer "post_id", limit: 4, null: false
    t.integer "tag_id",  limit: 4, null: false
  end

  add_index "blogo_taggings", ["tag_id", "post_id"], name: "index_blogo_taggings_on_tag_id_and_post_id", unique: true, using: :btree

  create_table "blogo_tags", force: :cascade do |t|
    t.string   "name",       limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blogo_tags", ["name"], name: "index_blogo_tags_on_name", unique: true, using: :btree

  create_table "blogo_users", force: :cascade do |t|
    t.string   "name",            limit: 255, null: false
    t.string   "email",           limit: 255, null: false
    t.string   "password_digest", limit: 255, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blogo_users", ["email"], name: "index_blogo_users_on_email", unique: true, using: :btree

  create_table "carrousels", force: :cascade do |t|
    t.string   "carrousel_image",  limit: 255
    t.integer  "position",         limit: 4
    t.string   "title",            limit: 255
    t.text     "description",      limit: 65535
    t.integer  "text_alignment",   limit: 4
    t.string   "title_color",      limit: 255
    t.string   "text_color",       limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "bg_image",         limit: 255
    t.string   "bg_text",          limit: 255
    t.text     "carousel_content", limit: 65535
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "client_logo", limit: 255
    t.string   "link",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "factories", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "factory_image", limit: 255
    t.integer  "position",      limit: 4
    t.string   "title",         limit: 255
    t.text     "description",   limit: 65535
    t.string   "factory_bg",    limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "bg_color",      limit: 255
  end

  create_table "headers", force: :cascade do |t|
    t.string   "bg_color",         limit: 255
    t.string   "bg_image",         limit: 255
    t.string   "link_color",       limit: 255
    t.string   "link_hover_color", limit: 255
    t.string   "links_width",      limit: 255
    t.string   "normal_image",     limit: 255
    t.string   "thin_image",       limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "border_color",     limit: 255
  end

  create_table "links", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "link",       limit: 255
    t.integer  "position",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "monologue_posts", force: :cascade do |t|
    t.boolean  "published"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id",      limit: 4
    t.string   "title",        limit: 255
    t.text     "content",      limit: 65535
    t.string   "url",          limit: 255
    t.datetime "published_at"
  end

  add_index "monologue_posts", ["url"], name: "index_monologue_posts_on_url", unique: true, using: :btree

  create_table "monologue_taggings", force: :cascade do |t|
    t.integer "post_id", limit: 4
    t.integer "tag_id",  limit: 4
  end

  add_index "monologue_taggings", ["post_id"], name: "index_monologue_taggings_on_post_id", using: :btree
  add_index "monologue_taggings", ["tag_id"], name: "index_monologue_taggings_on_tag_id", using: :btree

  create_table "monologue_tags", force: :cascade do |t|
    t.string "name", limit: 255
  end

  add_index "monologue_tags", ["name"], name: "index_monologue_tags_on_name", using: :btree

  create_table "monologue_users", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "email",           limit: 255
    t.string   "password_digest", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", force: :cascade do |t|
    t.string   "bg_color",   limit: 255
    t.string   "text_color", limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "picture",      limit: 255
    t.string   "rol",          limit: 255
    t.text     "passion",      limit: 65535
    t.string   "linkedin_url", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "position",     limit: 4
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "resource_id",   limit: 4
    t.string   "resource_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "sections", force: :cascade do |t|
    t.string   "title",            limit: 255
    t.string   "formated_title",   limit: 255
    t.text     "content",          limit: 65535
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "header_title",     limit: 255
    t.integer  "position",         limit: 4
    t.string   "background_color", limit: 255
    t.string   "title_color",      limit: 255
    t.string   "text_color",       limit: 255
    t.string   "image",            limit: 255
    t.string   "image_class",      limit: 255
    t.boolean  "section_blank"
    t.integer  "section_type",     limit: 4
    t.string   "bg_separator",     limit: 255
    t.string   "color_separator",  limit: 255
    t.string   "genies_title",     limit: 255
    t.string   "allies_title",     limit: 255
    t.string   "clients_title",    limit: 255
  end

  create_table "table_pay_links", force: :cascade do |t|
    t.string "link", limit: 255
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.integer "taggings_count", limit: 4,   default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "name",                   limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.integer  "role",                   limit: 4
    t.integer  "failed_attempts",        limit: 4
    t.string   "unlock_token",           limit: 255
    t.datetime "locked_at"
    t.string   "username",               limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4
    t.integer "role_id", limit: 4
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "utils", force: :cascade do |t|
    t.string   "type",       limit: 255
    t.string   "value",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

end
