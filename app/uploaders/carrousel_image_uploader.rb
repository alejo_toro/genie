# encoding: utf-8

class CarrouselImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick
 
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # include Cloudinary::CarrierWave

  # process :convert => 'jpg'
  # cloudinary_transformation :quality => Integer(80), :fetch_format => :auto, :flags => :lossy

  # def store_dir
  #   "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id.to_s[-3..-1]}/#{model.id.to_s[-6..-4]}/#{model.id}"
  #   #"uploads/yohan"
  # end

  # # Process files as they are uploaded:
  # version :carousel do
  #   cloudinary_transformation transformation: [
  #     {:width => 800, :height => 600, :crop => :fill, :fetch_format => :auto, :flags => :lossy},
  #     {:overlay => "web-card-base", :width => 800, :height => 600, :crop => :fill, :effect => "multiply"}
  #   ]
  # end

end
