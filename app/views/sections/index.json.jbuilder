json.array!(@sections) do |section|
  json.extract! section, :id, :title, :formated_title, :content
  json.url section_url(section, format: :json)
end
