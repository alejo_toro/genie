module ApplicationHelper
	def static_map_for(size)
		lat = ENV['GENIE_LATITUDE']
		lng = ENV['GENIE_LONGITUDE']

	    params = {
	    	:center => [lat, lng].join(","),
	    	:zoom => 18,
	    	:size => size,
	    	:markers => [lat, lng].join(","),
	    	:sensor => true,
	    	:scale => 2
	    }

	    query_string =  params.map{|k,v| "#{k}=#{v}"}.join("&")
	    image_tag "http://maps.googleapis.com/maps/api/staticmap?#{query_string}", :alt => "Génie Consultoría SAS", :class => "genie-location"
    end
end
