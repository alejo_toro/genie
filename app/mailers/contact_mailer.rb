class ContactMailer < ApplicationMailer
	default from: "genie@genieconsultoria.com"

	def contact_email(params)
		@user = params[:name]
		@email = params[:email]
		@subject = params[:subject]
		@message = params[:message]
		mail(to: "genie@genieconsultoria.com", subject: @subject)
	end
end
