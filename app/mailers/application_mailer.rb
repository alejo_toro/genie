class ApplicationMailer < ActionMailer::Base
	default from: "genie@genieconsultoria.com"
	layout 'mailer'
end
