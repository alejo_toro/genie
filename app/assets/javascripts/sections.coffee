# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('body').on 'mouseenter', '.factory', (e) ->
    $('.factory').removeClass 'hide'
    $('.factory-hover').addClass 'hide'
    $(this).addClass 'hide'
    $(this).parent().find('.factory-hover').removeClass 'hide'
    return

  return