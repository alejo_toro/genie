/**
 * cbpAnimatedHeader.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var cbpAnimatedHeader = (function() {

	var docElem = document.documentElement,
		header = document.querySelector( '.navbar-fixed-top' ),
		didScroll = false,
		changeHeaderOn = 300;

	function init() {
		window.addEventListener( 'scroll', function( event ) {
			if( !didScroll ) {
				didScroll = true;
				setTimeout( scrollPage, 300 );
			}
		}, false );
	}

	function scrollPage() {
		var sy = scrollY();
		// if ( sy >= changeHeaderOn ) {
		// 	$('.navbar-fixed-top').addClass('navbar-shrink');
		// 	$('.genie-logo').addClass('hide');
		// 	$('.genie-slogan').removeClass('hide');
		// }
		// else {
		// 	$('.navbar-fixed-top').removeClass('navbar-shrink');
		// 	$('.genie-logo').removeClass('hide');
		// 	$('.genie-slogan').addClass('hide');
		// }
		didScroll = false;
	}

	function scrollY() {
		return window.pageYOffset || docElem.scrollTop;
	}

	init();

})();