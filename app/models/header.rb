class Header < ActiveRecord::Base
  mount_uploader :bg_image, HeaderBgImageUploader
  mount_uploader :normal_image, HeaderNormalLogoImageUploader
  mount_uploader :thin_image, HeaderThinLogoImageUploader
end
