class Factory < ActiveRecord::Base
  mount_uploader :factory_image, FactoryImageUploader
  mount_uploader :factory_bg, FactoryBgUploader
  enum text_alignment: [:arriba, :abajo]
  default_scope { order(:position => :asc) }
end
