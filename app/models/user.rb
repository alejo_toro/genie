class User < ActiveRecord::Base
  rolify :before_add => :set_default_role
  enum role: [:user, :admin]
  # after_initialize :set_default_role, :if => :new_record?
  before_create :set_confirmation_token

  def set_default_role
    self.role ||= :user
  end

  def set_confirmation_token
    token = ([self.email] + SecureRandom.hex(6).chars).shuffle.join
    self.confirmation_token = token
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable
end
