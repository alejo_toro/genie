class Carrousel < ActiveRecord::Base
  mount_uploader :carrousel_image, CarrouselImageUploader
  enum text_alignment: [:arriba, :abajo]
  default_scope { order(:position => :asc) }
end
