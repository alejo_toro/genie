class Section < ActiveRecord::Base
  enum section_type: [:carrusel, :fabricas, :aliados]

  mount_uploader :image, SectionImageUploader
  mount_uploader :bg_separator, BgSeparatorImageUploader
  
  before_save :set_formated_title, if: Proc.new { |section| section.title_changed? }

  default_scope { order(:position => :asc) }

  def set_formated_title
    self.formated_title = self.header_title.downcase.gsub(/[\d\W]/, "-")
  end
end
