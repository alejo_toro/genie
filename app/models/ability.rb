class Ability
  include CanCan::Ability

  def initialize(user)
    can :access, :rails_admin
    can :dashboard
    if user
      if user.role == "admin"
        can :manage, :all
      else
        can :manage, [Section]
      end
    end
  end
end
