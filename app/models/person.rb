class Person < ActiveRecord::Base
  mount_uploader :picture, PeoplePictureUploader
  default_scope { order(:position => :asc) }
end
