class Address < ActiveRecord::Base

	scope :main_address, -> { where(main_address: true).first }

	def address_with_coordinates
		Address.where(:lat.ne => [nil, "", " "], :lng.ne => [nil, "", " "]).last
	end
end
